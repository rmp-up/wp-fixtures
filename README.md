![](https://img.shields.io/badge/PHP-7.0%20--%208.2-blue?style=for-the-badge&logo=php)
![](https://img.shields.io/badge/WordPress-4.7%20--%206.2-blue?style=for-the-badge&logo=wordpress)

[![Build Status](https://gitlab.com/rmp-up1/wp-fixtures/badges/release/0.8/pipeline.svg)](https://gitlab.com/rmp-up1/wp-fixtures)
[![Coverage Status](https://coveralls.io/repos/github/rmp-up/wp-fixtures/badge.svg)](https://coveralls.io/github/rmp-up/wp-fixtures)

# WordPress Fixture Generator

> Expressive fixtures persistence in WordPress (and some plugins).

GDPR and fluent data is a PITA while developing solutions
to specific problems for your customers.
[Faker](https://github.com/fzaninotto/Faker)
and [Alice](https://github.com/nelmio/alice)
is already there for Symfony, Nette, Zend and now for WordPress to ...

* ... create data using wp-cli or plain PHP.
* ... generate **tons of pages, posts, comments, products
      and more** with just a few YAML-lines
* ... seed WordPress with **specific use-cases for testing**

Overall the goal is **simplicity** and **no time-wasting crap** (for me and you)
which we achieve by using raw WordPress structures in the YAML instead of
reinventing the wheel with custom classes.


## Install

Download or just

    composer install --dev rmp-up/wp-fixtures

We mostly require what Alice needs
([see Packagist.org for more details](https://packagist.org/packages/rmp-up/wp-fixtures)) 

* PHP >= 7.0
* WordPress >= 4.7
* Optional: wp-cli 2 (eats YAML and seeds the DB for you)

Currently tested for and works with:

|  PHP  | WordPress | Supported until |
|:-----:|:---------:|:---------------:|
|  8.2  | 5.9 - 6.2 |   08 Dec 2025   |
|  8.1  | 5.9 - 6.2 |   25 Nov 2024   |
|  8.0  | 5.5 - 6.2 |   26 Nov 2023   |
| _7.4_ |  _4.9+_   |        -        |
| _7.3_ |  _4.8+_   |        -        |
| _7.2_ |  _4.8+_   |        -        |
| _7.1_ |  _4.7+_   |        -        |
| _7.0_ |  _4.7+_   |        -        |


## Usage

See how a small Yaml-File can seed the database with tons of entries
or [read the documentation](https://github.com/rmp-up/wp-fixtures/releases)
to find out more about all the entities and possibilities.

### Create 10 posts from random users with 100 comments

```yaml
WP_Post:
  # or with abbreviated field-names
  post_{1..10}:
    author: '<wpUser()>'
    title: '<sentence()>'
    content: '<realText()>'

WP_Comment:
  # 100 comments randomly spread among posts
  comment_{1..100}:
    post_ID: '@post_<numberBetween(1,10)>'
    content: '<realText()>'
```

### Create content with meta-data

```yaml
WP_Post:
  page_1:
    # Well known wp_insert_post() structure
    post_type: page
    post_title: Imprint
    post_content: |
      Beth Doe
      Meestreet 42
      1337 Muskegon
    meta_input:
      simple: 1
      flat: # will append like add_post_meta
      - penny
      - claire
      - emily
      - lucy
      complex: # will be serialized
        company: True TV
        best: Ellen
```

### Create users with 300 subscribers for testing

```yaml
WP_User:
  my-admin:
    role: 'administrator'
    user_pass: snap
    meta_input:
      wf_user-settings: editor=html

  my-author:
    role: 'author'
    user_pass: snap

  my-subscriber_{1..300}:
    role: 'subscriber'
    user_pass: snap
```


### Assert WordPress options for testing

```yaml
options:
  default_options:
    active_plugins:
      - akismet/akismet.php
      - hello.php
    blogname: Yet another blob
    template: twentynineteen
    stylesheet: twentynineteen
    ping_sites: ''
```


### Other possible entities

* Options
* WP_Comment
* WP_Post
* WP_Role
* WP_Site
* WP_Term
* WP_User

For more information [read the documentation](https://github.com/rmp-up/wp-fixtures/releases)
attached to minor releases.

## License

Copyright 2023 Pretzlaw (rmp-up.de)

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software
and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
