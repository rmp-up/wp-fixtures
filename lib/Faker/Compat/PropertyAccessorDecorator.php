<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * AbstractPropertyAccessor.php
 *
 */

declare(strict_types=1);

namespace RmpUp\WordPress\Fixtures\Faker\Compat;

use Closure;
use ReflectionProperty;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * AbstractPropertyAccessor
 */
trait PropertyAccessorDecorator
{
	/**
	 * @var PropertyAccessorInterface
	 */
	protected $decoratedPropertyAccessor;

	public function __construct(PropertyAccessorInterface $decoratedPropertyAccessor)
	{
		$this->decoratedPropertyAccessor = $decoratedPropertyAccessor;
	}

	private function setTheValue(&$objectOrArray, $propertyPath, $value)
	{
		try {
			$this->decoratedPropertyAccessor->setValue($objectOrArray, $propertyPath, $value);
		} catch (NoSuchPropertyException $exception) {
			if (false === $this->isValid($objectOrArray, $propertyPath)) {
				throw $exception;
			}

			$setPropertyClosure = Closure::bind(
				function ($object) use ($propertyPath, $value) {
					$object->{$propertyPath} = $value;
				},
				$objectOrArray,
				$objectOrArray
			);

			$setPropertyClosure($objectOrArray);
		}
	}

	private function getTheValue($objectOrArray, $propertyPath): mixed
	{
		try {
			return $this->decoratedPropertyAccessor->getValue($objectOrArray, $propertyPath);
		} catch (NoSuchPropertyException $exception) {
			$setPropertyClosure = Closure::bind(
				function ($object) use ($propertyPath) {
					return $object->{$propertyPath};
				},
				$objectOrArray,
				$objectOrArray
			);

			return $setPropertyClosure($objectOrArray);
		}
	}

	private function isItWritable($objectOrArray, $propertyPath): bool
	{
		$setPropertyClosure = Closure::bind(
			function ($object) use ($propertyPath) {
				return isset($object->{$propertyPath});
			},
			$objectOrArray,
			$objectOrArray
		);

		return $setPropertyClosure($objectOrArray)
			|| $this->decoratedPropertyAccessor->isWritable($objectOrArray, $propertyPath);
	}

	private function isItReadable($objectOrArray, $propertyPath): bool
	{
		$setPropertyClosure = Closure::bind(
			function ($object) use ($propertyPath) {
				return isset($object->{$propertyPath});
			},
			$objectOrArray,
			$objectOrArray
		);

		return $setPropertyClosure($objectOrArray)
			|| $this->decoratedPropertyAccessor->isReadable($objectOrArray, $propertyPath);
	}

	/**
	 * @param $objectOrArray
	 * @param $propertyPath
	 * @return bool
	 */
	private function isValid($objectOrArray, $propertyPath): bool
	{
		return is_object($objectOrArray) && (
			property_exists($objectOrArray, $propertyPath)
			|| $this->isWritable($objectOrArray, (string)$propertyPath)
		);
	}
}
