<?php /** @noinspection PhpLanguageLevelInspection */
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PropertyAccessor.php
 *
 */

declare(strict_types=1);

namespace RmpUp\WordPress\Fixtures\Faker\Compat;

use Closure;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPathInterface;

/**
 * PropertyAccessor
 *
 * @deprecated Drop when PHP < 8.0 is no longer supported.
 */
final class PropertyAccessor_5 implements PropertyAccessorInterface
{
	use PropertyAccessorDecorator;

	public function setValue(&$objectOrArray, $propertyPath, $value) {
		$this->setTheValue($objectOrArray, $propertyPath, $value);
	}

	public function getValue($objectOrArray, $propertyPath): mixed {
		return $this->getTheValue($objectOrArray, $propertyPath);
	}

	public function isWritable($objectOrArray, $propertyPath): bool {
		return $this->isItWritable($objectOrArray, $propertyPath);
	}

	public function isReadable($objectOrArray, $propertyPath): bool {
		return $this->isItReadable($objectOrArray, $propertyPath);
	}
}
