<?php /** @noinspection PhpLanguageLevelInspection */
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PropertyAccessor.php
 *
 */

declare(strict_types=1);

namespace RmpUp\WordPress\Fixtures\Faker\Compat;

use Closure;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPathInterface;

/**
 * PropertyAccessor
 */
final class PropertyAccessor_6 implements PropertyAccessorInterface
{
	use PropertyAccessorDecorator;

	public function setValue(object|array &$objectOrArray, string|PropertyPathInterface $propertyPath, mixed $value) {
		$this->setTheValue($objectOrArray, $propertyPath, $value);
	}

	public function getValue(object|array $objectOrArray, string|PropertyPathInterface $propertyPath): mixed {
		return $this->getTheValue($objectOrArray, $propertyPath);
	}

	public function isWritable(object|array $objectOrArray, string|PropertyPathInterface $propertyPath): bool {
		return $this->isItWritable($objectOrArray, $propertyPath);
	}

	public function isReadable(object|array $objectOrArray, string|PropertyPathInterface $propertyPath): bool {
		return $this->isItReadable($objectOrArray, $propertyPath);
	}
}
