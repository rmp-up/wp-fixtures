<?php

namespace RmpUp\WordPress\Fixtures\Faker;

use Composer\InstalledVersions;
use RmpUp\WordPress\Fixtures\Faker\Compat\PropertyAccessor_5;
use RmpUp\WordPress\Fixtures\Faker\Compat\PropertyAccessor_6;

$_version = InstalledVersions::getVersion('symfony/property-access');
$_class = '\\RmpUp\\WordPress\\Fixtures\\Faker\\PropertyAccessor';

if (version_compare($_version, '6.0.0', '>=')) {
    class_alias(PropertyAccessor_6::class, $_class);
    return;
}

class_alias(PropertyAccessor_5::class, $_class);
