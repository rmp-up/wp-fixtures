<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * TestCase.php
 *
 * LICENSE: This source file is created by the company around M. Pretzlaw
 * located in Germany also known as rmp-up. All its contents are proprietary
 * and under german copyright law. Consider this file as closed source and/or
 * without the permission to reuse or modify its contents.
 * This license is available through the world-wide-web at the following URI:
 * https://rmp-up.de/license-generic.txt . If you did not receive a copy
 * of the license and are unable to obtain it through the web, please send a
 * note to mail@rmp-up.de so we can mail you a copy.
 *
 * @package   WPFixtures
 * @copyright 2020 Pretzlaw
 * @license   https://rmp-up.de/license-generic.txt proprietary
 */

declare(strict_types=1);

namespace RmpUp\WordPress\Fixtures\Test;

use RmpUp\WordPress\Fixtures\Faker\LoaderFactory;
use RmpUp\WordPress\Fixtures\Helper\FixturesAutoloadTrait;

/**
 * TestCase
 *
 * @copyright 2020 Pretzlaw (https://rmp-up.de)
 *
 * @deprecated 0.10.0 Please use BasicTestCase or other more specific bases.
 */
class TestCase extends FixtureTestCase
{
}
