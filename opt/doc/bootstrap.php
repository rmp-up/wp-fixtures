<?php

const MY_PLUGIN_DIR = __DIR__;

$_SERVER['HTTP_HOST'] = '10.211.3.71'; // DOMAIN_CURRENT_SITE in wp-config.php

define('_BASE_DIR', dirname(__DIR__, 2));
putenv('WP_DIR=' . _BASE_DIR . '/srv');

require_once _BASE_DIR . '/vendor/autoload.php';
require_once _BASE_DIR . '/etc/wp/vendor/autoload.php';

if (defined('__PHPSTAN_RUNNING__') && __PHPSTAN_RUNNING__) {
    require_once _BASE_DIR . '/etc/phpstan/vendor/autoload.php';
    require_once _BASE_DIR . '/etc/phpstan/vendor/pretzlaw/wp-integration-test/bootstrap.php';
} else {
    require_once _BASE_DIR . '/etc/phpunit/vendor/autoload.php';
    require_once _BASE_DIR . '/etc/phpunit/vendor/pretzlaw/wp-integration-test/bootstrap.php';
}

class Mirror extends \stdClass {}

class_alias(Mirror::class, 'SomeThing');

// Enable after bootstrapping WP - to not confuse bootstrap of installed plugins
const WP_CLI = true;

function rmp_wp_fixture_all_caps($allcaps, $caps)
{
    foreach ($caps as $cap) {
        // allow all.
        $allcaps[$cap] = true;
    }

    return $allcaps;
}

add_filter('user_has_cap', 'rmp_wp_fixture_all_caps', 10, 2);

$preloadFunctions = [
    ABSPATH . 'wp-admin/includes/ms.php',
    ABSPATH . 'wp-admin/includes/user.php',
    ABSPATH . 'wp-includes/ms-functions.php',
    ABSPATH . 'wp-includes/ms-site.php',
];
foreach ($preloadFunctions as $functionDefinitionFile) {
    if (file_exists($functionDefinitionFile)) {
        require_once $functionDefinitionFile;
    }
}

spl_autoload_register(
    static function ($class): bool {
        if (false === defined('ABSPATH') || 0 !== strpos($class, 'WP_')) {
            return false;
        }

        $classFile = strtolower(
            'class-' . str_replace('_', '-', $class) . '.php'
        );

        $paths = [
            ABSPATH . '/wp-includes/',
            ABSPATH . '/wp-includes/customizer/',
            ABSPATH . '/wp-includes/rest-api/',
            ABSPATH . '/wp-includes/rest-api/search/',
            ABSPATH . '/wp-includes/rest-api/endpoints/',
            ABSPATH . '/wp-includes/rest-api/fields/',
            ABSPATH . '/wp-includes/sitemaps/',
            ABSPATH . '/wp-admin/includes/',
        ];

        foreach ( $paths as $path ) {
            if ( file_exists( $path . $classFile ) ) {
                require_once $path . $classFile;
                return true;
            }
        }

        return false;
    }
);

require_once __DIR__ . '/../../lib/compat.php';
